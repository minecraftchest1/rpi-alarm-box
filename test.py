#!/usr/bin/env python3

from liblcd import LCD
from time import sleep
import RPi.GPIO as GPIO

# Rotary A Pin
RoAPin = 20
# Rotary B Pin
RoBPin = 21
# Rotary Switch Pin
RoSPin = 16

def setup():
    global counter
    global Last_RoB_Status, Current_RoB_Status
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(RoAPin, GPIO.IN)
    GPIO.setup(RoBPin, GPIO.IN)
    GPIO.setup(RoSPin,GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Set up a falling edge detect to callback clear
    # GPIO.add_event_detect(RoSPin, GPIO.FALLING, callback=clear)

    # Set up a counter as a global variable
    counter = 0
    Last_RoB_Status = 0
    Current_RoB_Status = 0

def destroy(self):
    #print ("clean up used_gpio")
    self.GPIO.cleanup(self.used_gpio)

def loop():
    #pdb.set_trace()
    global lcd
    lcd = LCD()
    while True:
        print(GPIO.input(RoSPin))
        sleep(0.1)

def destroy():
    lcd.destroy()

# If run this script directly, do:
if __name__ == '__main__':
	setup()
	try:
		loop()
	# When 'Ctrl+C' is pressed, the child program 
	# destroy() will be  executed.
	except KeyboardInterrupt:
		destroy()

