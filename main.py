#!/usr/bin/env python3

from liblcd import LCD
from time import sleep
import RPi.GPIO as GPIO
import pdb

# Rotary A Pin
RoAPin = 20
# Rotary B Pin
RoBPin = 21
# Rotary Switch Pin
RoSPin = 16

Btn1 = 0

def setup():
    global counter
    global Last_RoB_Status, Current_RoB_Status
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(RoAPin, GPIO.IN)
    GPIO.setup(RoBPin, GPIO.IN)
    GPIO.setup(RoSPin,GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Set up a falling edge to detect btn1 press
    GPIO.add_event_detect(RoSPin, GPIO.FALLING, callback=btn1)

    # Set up a counter as a global variable
    counter = 0
    Last_RoB_Status = 0
    Current_RoB_Status = 0
    #Btn1 = 0

def menuLibrary():
    lcd.clear()
    lcd.message("To be created")

def menuMain():
    exit = False
    while exit != True:
        print("Library")


def destroy(self):
    #print ("clean up used_gpio")
    self.GPIO.cleanup(self.used_gpio)

def loop():
    global Btn1
    #pdb.set_trace()
    global lcd
    lcd = LCD()
    while True:
        # lcd.clear()
        # lcd.message(" LCD 1602 Test \n123456789ABCDEF")
        # sleep(2)
        # lcd.clear()
        # lcd.message("   SUNFOUNDER \nHello World ! :)")
        # sleep(2)
        # lcd.clear()
        # lcd.message("Welcom to --->\n  sunfounder.com")
        # sleep(2)
        #pdb.set_trace()
        usrInput = input()
        #pdb.set_trace()
        #print(usrInput)
        if usrInput == 0:
            print("Direction 1")
        if usrInput == 1:
            print("Direction 2")
        # if usrInput == 2:
        #     print("Btn1 Pressed")
        if Btn1 == 1:
            print("Button Pressed")
            Btn1 = 0
        sleep(0.01)


def destroy():
    lcd.destroy()

# Define a function to deal with rotary encoder
def input():
    global counter
    global Last_RoB_Status, Current_RoB_Status, Last_RoS_Status, Current_RoS_Status

    flag = 0
    rosFlag = 0
    Last_RoB_Status = GPIO.input(RoBPin)
    Last_RoS_Status = GPIO.input(RoSPin)
    # When RoAPin level changes
    while(not GPIO.input(RoAPin)):
        Current_RoB_Status = GPIO.input(RoBPin)
        flag = 1
    # while(GPIO.input(RoSPin) == 1):
    #     rosFlag = 0
    
    if flag == 1:
        # Reset flag
        flag = 0
        if (Last_RoB_Status == 0) and (Current_RoB_Status == 1):
            return 0
        if (Last_RoB_Status == 1) and (Current_RoB_Status == 0):
            return 1

    # if (rosFlag == 0):
    #     if (GPIO.input(RoSPin) == 0):
    #         rosFlag = 1
    #         return 2
    # if (rosFlag == 1):
    #     if (GPIO.input(RoSPin) == 1):
    #         rosFlag = 0

    # while(not GPIO.input(RoSPin)):
    #     Current_RoS_Status = GPIO.input(RoSPin)
    #     rosFlag = 1
    #
    # if rosFlag == 1:
    #     rosFlag = 0
    #     if (Last_RoS_Status == 1) and (Current_RoS_Status == 0):
    #         return 2

# Define a callback function on switch, to clean "counter"
def btn1(ev=None):
    global Btn1
    Btn1=1

def main():
	while True:
		loop()

def destroy():
	# Release resource
	GPIO.cleanup()  

# If run this script directly, do:
if __name__ == '__main__':
	setup()
	try:
		main()
	# When 'Ctrl+C' is pressed, the child program 
	# destroy() will be  executed.
	except KeyboardInterrupt:
		destroy()

